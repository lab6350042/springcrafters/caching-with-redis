FROM openjdk:17.0.1-jdk-slim AS build

WORKDIR /app

COPY . .

RUN ./mvnw package

RUN cp target/*.jar app.jar

FROM openjdk:17.0.1-jdk-slim

RUN mkdir -p /opt/app

WORKDIR /opt/app

COPY --from=build /app/app.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]