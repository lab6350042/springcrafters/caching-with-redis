# Projeto Java com Spring Boot

Este projeto é um template para projetos Java com Spring Boot. 
Ele fornece uma estrutura básica para o desenvolvimento de aplicações web e API RESTful.

## Pré-requisitos

* Java 17
* Maven 3.8.7

## Instalação

1. Clone o repositório:

`git clone https://gitlab.com/[seu-usuário]/[nome-do-projeto].git`


2. Acesse o diretório do projeto:

`cd [nome-do-projeto]`


3. Instale as dependências:

`mvn install`


## Execução

Para executar o projeto, execute o seguinte comando:

`mvn spring-boot:run`


O projeto será executado na porta 8080.

## Contribições

Contribuições são bem-vindas. Para contribuir, faça um fork do repositório e envie um pull request.

## Licença

Este projeto está licenciado sob a licença MIT.
