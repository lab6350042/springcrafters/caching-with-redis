package com.example.app.rest;

import com.example.app.domain.Product;
import com.example.app.repository.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product")
@AllArgsConstructor
@CacheConfig(cacheNames = "product")
public class ProductRest {
    private final ProductRepository productRepository;

    @GetMapping("/{id}")
    @Cacheable(unless = "#result.price > 1000")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(
                productRepository.findById(id).orElseThrow(EntityNotFoundException::new)
        );
    }

    @PutMapping("/{id}")
    @CachePut
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Product resource) {
        if (!productRepository.existsById(id)) {
            throw new EntityNotFoundException();
        }

        return ResponseEntity.ok(productRepository.save(resource));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @CacheEvict
    public void delete(@PathVariable Long id) {
        if (!productRepository.existsById(id)) {
            throw new EntityNotFoundException();
        }

        productRepository.deleteById(id);
    }
}
